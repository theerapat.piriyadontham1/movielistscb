import React, {useContext} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Context as FavoriteContext} from '../context/favoriteContext';
import {Button} from 'react-native';

const FavoriteButton = () => {
  const navigation = useNavigation();
  const {state} = useContext(FavoriteContext);

  var isFav = false;
  const checkFav = () => {
    if (state.length === 0) {
      isFav = true;
    } else {
      isFav = false;
    }
  };
  checkFav();
  return (
    <Button
      disabled={isFav ? true : false}
      title="Favorite"
      onPress={() => {
        navigation.navigate('FavoriteScene');
      }}
    />
  );
};
export default FavoriteButton;
