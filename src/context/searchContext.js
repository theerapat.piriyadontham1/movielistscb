import createDataContext from './createContext';

//Reducer
const searchReducer = (state, {type, payload}) => {
  switch (type) {
    case 'add_history':
      return [...state, payload];
    // {title:payload} =>> state.title
    //payload =>> state
    case 'remove_history':
      return state.filter(item => item !== payload);
    default:
      return state;
  }
};

//action
const addHistory = dispatch => {
  return title => {
    dispatch({type: 'add_history', payload: title});
  };
};
const removeHistory = dispatch => {
  return title => {
    dispatch({type: 'remove_history', payload: title});
  };
};

export const {Context, Provider} = createDataContext(
  searchReducer,
  {addHistory, removeHistory},
  [],
);
