import React from 'react';
import {useNavigation} from '@react-navigation/native';

import Favorite from '../../component/Favorite';
const FavoriteScene = () => {
  const navigation = useNavigation();
  navigation.setOptions({
    title: 'Favorite Movies',
  });
  return <Favorite />;
};
export default FavoriteScene;
