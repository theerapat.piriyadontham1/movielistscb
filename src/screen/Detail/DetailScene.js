import React, {useState, useContext} from 'react';
import {Text, View, StyleSheet, Image, Button} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {Context as favContext} from '../../context/favoriteContext';

const DetailScene = props => {
  const {data} = props.route.params;
  var Isfav = false;
  const navigation = useNavigation();
  const {state, addFavorite, removeFavorite} = useContext(favContext);

  const checkFavorite = () => {
    if (state !== []) {
      state.map(item => {
        if (item.id === data.id) {
          Isfav = true;
        } else {
          Isfav = false;
        }
      });
    }
  };
  checkFavorite();
  navigation.setOptions({
    title: '',
    headerRight: () => (
      <Button
        title="Back to Search"
        onPress={() => {
          navigation.navigate('SearchScene');
        }}
      />
    ),
  });
  return (
    <View>
      <Image
        style={[styles.imageDetail, styles.imageAlign]}
        source={{uri: `https://image.tmdb.org/t/p/w92${data.poster_path}`}}
      />
      <Text style={styles.titleText}>{data.title}</Text>
      <Text style={styles.subtitleText}>
        Average Votes : {data.vote_average}
      </Text>
      <Text style={styles.defualtText}>{data.overview}</Text>
      <TouchableOpacity
        onPress={() => {
          Isfav ? removeFavorite(data) : addFavorite(data);
          // console.log(Isfav);
        }}
        style={Isfav ? styles.appButtonContainer2 : styles.appButtonContainer}>
        <Text style={styles.appButtonText}>
          {Isfav ? 'Unfavorite' : 'Favorite'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  imageDetail: {
    width: 200,
    height: 300,
    alignContent: 'center',
  },
  imageHome: {
    width: 150,
    height: 180,
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  imageAlign: {
    alignSelf: 'center',
    margin: 10,
    marginBottom: 20,
  },
  titleText: {
    fontWeight: 'bold',
    fontSize: 22,
    margin: 5,
    marginLeft: 10,
  },
  subtitleText: {
    fontWeight: 'normal',
    fontSize: 16,
    margin: 5,
    marginLeft: 10,
  },
  defualtText: {
    fontWeight: 'normal',
    fontSize: 14,
    marginTop: 10,
    marginLeft: 10,
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: 'orange',
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 50,
  },
  appButtonContainer2: {
    elevation: 8,
    backgroundColor: '#FAD7A0',
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 50,
  },
  appButtonText: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
export default DetailScene;
