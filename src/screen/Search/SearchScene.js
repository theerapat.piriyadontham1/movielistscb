import React, {useState, useContext} from 'react';
import {View, Button, Text, StyleSheet} from 'react-native';
import {
  FlatList,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {Context as searchContext} from '../../context/searchContext';
import FavoriteButton from '../../component/FavoriteButton';

const SearchScene = () => {
  const navigation = useNavigation();
  const [result, setResult] = useState();
  const {removeHistory, addHistory, state} = useContext(searchContext);
  const listSize = 5;

  navigation.setOptions({
    title: '',
    headerRight: () => <FavoriteButton />,
  });
  const adjustHistory = () => {
    const searchItem = state.find(item => item === result);
    if (state.length < listSize) {
      if (searchItem === undefined) {
        addHistory(result);
      } else {
        removeHistory(result);
        addHistory(result);
      }
    } else {
      removeHistory(state[listSize - 1]);
      addHistory(result);
    }
  };

  return (
    <View style={styles.container}>
      <View style={[styles.row, styles.txtInputBG]}>
        <View style={[styles.txtBorder]}>
          <TextInput
            style={[styles.defaultText]}
            placeholder="  Search your movie."
            placeholderTextColor="grey"
            onChangeText={text => setResult(text)}
            value={result}
          />
        </View>

        <View style={{justifyContent: 'center'}}>
          <Button
            title="Search"
            onPress={() => {
              // addHistory(result);
              adjustHistory(result);
              navigation.navigate('ResultScene', {data: result});
              setResult('');
            }}
          />
        </View>
      </View>

      <FlatList
        data={state}
        keyExtractor={item => item}
        renderItem={({item}) => {
          return (
            <TouchableWithoutFeedback
              style={styles.line}
              onPress={() => {
                removeHistory(item);
                addHistory(item);
                navigation.navigate('ResultScene', {data: item});
              }}>
              <Text style={styles.txtHistory}>{item}</Text>
            </TouchableWithoutFeedback>
          );
        }}
      />
      <FlatList />
    </View>
  );
};

const styles = StyleSheet.create({
  defaultText: {
    color: 'black',
    marginLeft: 4,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 5,
  },
  txtBorder: {
    borderWidth: 1,
    borderRadius: 100,
    height: 40,
    margin: 4,
    width: '80%',
    backgroundColor: '#F7F9F9',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
    alignContent: 'center',
  },
  txtInputBG: {
    backgroundColor: 'lightgray',
  },
  column: {
    flexDirection: 'column',
    alignContent: 'center',
    margin: 5,
  },
  txtHistory: {
    fontSize: 16,
    margin: 12,
    fontWeight: 'bold',
  },
  line: {
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
  },
});

export default SearchScene;
