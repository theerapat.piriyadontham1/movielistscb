import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SearchScene from './src/screen/Search/SearchScene';
import FavoriteScene from './src/screen/Favorite/FavoriteScene';
import DetailScene from './src/screen/Detail/DetailScene';

import {Provider as SearchProvider} from './src/context/searchContext';
import {Provider as FVRProvider} from './src/context/favoriteContext';
import ResultScene from './src/screen/Result/ResultScene';

const Stack = createStackNavigator();

const App = () => {
  return (
    <FVRProvider>
      <SearchProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="SearchScene">
            <Stack.Screen name="SearchScene" component={SearchScene} />
            <Stack.Screen name="FavoriteScene" component={FavoriteScene} />
            <Stack.Screen name="ResultScene" component={ResultScene} />
            <Stack.Screen name="DetailScene" component={DetailScene} />
          </Stack.Navigator>
        </NavigationContainer>
      </SearchProvider>
    </FVRProvider>
  );
};

export default App;
